import React, { Component } from 'react';
import { View } from 'react-native';
import { StackNavigator } from 'react-navigation';
import LoginView from './app/views/loginView';
import HomeView from './app/views/homeView';

export default App = StackNavigator({
  Login: { screen: LoginView },
  Home: { screen: HomeView },
})


