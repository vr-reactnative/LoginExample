//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

// create a component
class TextAula extends Component {
    render() {
        return (
            <TextInput style={estilos.caixaTexto} underlineColorAndroid="transparent" placeholderTextColor={this.props.placeholderColor} onChangeText={(value) => this.value = value} {...this.props}  />
        );
    }
}

// define your styles
const estilos = StyleSheet.create({
    caixaTexto: {
        width: 200,
        marginTop: 30,
        height: 35,
        color: '#fff',
        backgroundColor: 'transparent',
        borderBottomWidth: 0.5,
        borderColor: '#FFF'
    }
});

//make this component available to the app
export default TextAula;
