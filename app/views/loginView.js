//import liraries
import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Image, 
    TextInput, 
    TouchableOpacity,
    StatusBar,
    KeyboardAvoidingView,
    ActivityIndicator
  } from 'react-native';
  import TextAula from '../components/textAula';

// create a component
class LoginView extends Component {
    static navigationOptions = {
        header: null
    }

    state = {
        user: null,
        pass: null,
        isLoading: false
    }

    handleLogin()
    {
        const { txtUser, txtPass } = this.refs;

        // altera o status para carregando
        this.setState({ isLoading: true });

        fetch('http://aulareact.getsandbox.com/login', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              user: txtUser.value,
              pass: txtPass.value,
            })
        }).then((response) => response.json())
        .then((responseJson) => {
            this.setState({ isLoading: false });
            if(responseJson.status == "ok")
            {
                this.navigation.navigate('Home');
            }
            else
            {
                alert('Login Incorreto!');
            }            
        })

        
    }

    render() {
        const logoImage = require('../assets/images/logo.png');
        const backgroundImage = require('../assets/images/background.jpg');
        this.navigation = this.props.navigation;

        return (
            <View style={estilos.container}>
                <StatusBar barStyle="light-content" />
                <Image style={estilos.backgroundImage} source={backgroundImage} />
                <View style={estilos.overlay} />
                <KeyboardAvoidingView behavior="position" style={estilos.containerLogin}>
                  <Image style={estilos.logoImage} source={logoImage} />
                  
                  {
                     (this.state.isLoading) ?
                     (<ActivityIndicator color="#FFF" />) :
                     (
                        <View>
                          <TextAula ref="txtUser" keyboardType="default" autoCapitalize="none" autoCorrect={false} placeholder="user" placeholderColor="#aaa"  />
                          <TextAula ref="txtPass" secureTextEntry={true} placeholder="password" placeholderColor="#aaa" />

                          <TouchableOpacity onPress={this.handleLogin.bind(this)} style={estilos.loginButton}>
                              <Text style={estilos.loginText}>Login</Text>
                          </TouchableOpacity>
                        </View>
                     )
                  }
                </KeyboardAvoidingView>
            </View>
        );
    }
}

const estilos = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    containerLogin: {
      position: 'absolute'
    },
    overlay: {
      width: '100%',
      height: '100%',
      position: 'absolute',
      backgroundColor: '#2d2d2d',
      opacity: 0.95
    },
    loginButton: {
      backgroundColor: 'transparent',
      width: 200,
      height: 30,
      alignItems: 'center',
      justifyContent: 'center',
      borderWidth: 0.5,
      borderColor: '#fff',
      borderRadius: 5,
      marginTop: 30,
    },
    loginText: {
      color: '#fff',
      fontSize: 15
    },
    backgroundImage: {
      width: '100%',
      height: '100%',
      position: 'absolute'
    },
    caixaTexto: {
      width: 200,
      marginTop: 30,
      height: 35,
      color: '#fff',
      backgroundColor: 'transparent',
      borderBottomWidth: 0.5,
      borderColor: '#FFF'
    },
    logoImage: {
      width: 150,
      height: 150,
      marginBottom: 80,
      resizeMode: 'contain'
    },
    estiloTexto: {
      backgroundColor: 'transparent',
      color: '#FFF',
      fontSize: 18,
      width: 200
    }
  });

//make this component available to the app
export default LoginView;
